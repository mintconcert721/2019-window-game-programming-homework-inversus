#include "월목_2014180035_이재원.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // 메모리 누수 체크

	HWND hWnd;
	MSG Message;
	WNDCLASSEX WndClass;
	g_hInstance = hInstance;

	// 윈도우 클래스 구조체 값 설정
	WndClass.cbSize = sizeof(WndClass);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = (WNDPROC)WndProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.lpszMenuName = NULL;
	WndClass.lpszClassName = lpszClass;
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// 윈도우 클래스 등록
	RegisterClassEx(&WndClass);

	RECT rt = { 0, 0, WINDOW_X, WINDOW_Y };
	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);

	// 윈도우 생성
	hWnd = CreateWindow(lpszClass, lpszWindowName, WS_OVERLAPPEDWINDOW, 100, 100,
		rt.right - rt.left, rt.bottom - rt.top, NULL, (HMENU)NULL, hInstance, NULL);

	// 윈도우 출력
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	// 이벤트 루프 처리
	while (GetMessage(&Message, 0, 0, 0))
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}

	return Message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc, memdc;
	static RECT crt; // 직사각형모양 범위 선언
	PAINTSTRUCT ps;

	static HBITMAP hBitmap, hOldBitmap;
	static char str[256];
	static char str2[256];
	static char str3[256];
	static char str4[256];
	static HFONT hFont, hOldFont;

	// 메시지 처리하기
	switch (uMsg)
	{
	case WM_CREATE:
	{
		GetClientRect(hWnd, &crt);

		for (int j = 0; j < TILE_NUMBER - 6; j++)
		{
			for (int i = 0; i < TILE_NUMBER; i++)
			{
				if (j == 2 || j == (TILE_NUMBER - 6 - 1 - 2))
				{
					if (i >= 6 && i <= 13)
						objectGroup[i][j].Init(WINDOW_X_CENTER + (i - (TILE_NUMBER / 2)) * 50, WINDOW_Y_CENTER + (j - ((TILE_NUMBER - 6) / 2)) * 50, false);
					else
						objectGroup[i][j].Init(WINDOW_X_CENTER + (i - (TILE_NUMBER / 2)) * 50, WINDOW_Y_CENTER + (j - ((TILE_NUMBER - 6) / 2)) * 50, false);
				}
				else if (j == 3 || j == (TILE_NUMBER - 6 - 1 - 3))
				{
					if (i >= 5 && i <= 14)
						objectGroup[i][j].Init(WINDOW_X_CENTER + (i - (TILE_NUMBER / 2)) * 50, WINDOW_Y_CENTER + (j - ((TILE_NUMBER - 6) / 2)) * 50, false);
					else
						objectGroup[i][j].Init(WINDOW_X_CENTER + (i - (TILE_NUMBER / 2)) * 50, WINDOW_Y_CENTER + (j - ((TILE_NUMBER - 6) / 2)) * 50, false);
				}
				else if ((j >= 4 && j <= 9))
				{
					if (i >= 4 && i <= 15)
						objectGroup[i][j].Init(WINDOW_X_CENTER + (i - (TILE_NUMBER / 2)) * 50, WINDOW_Y_CENTER + (j - ((TILE_NUMBER - 6) / 2)) * 50, false);
					else
						objectGroup[i][j].Init(WINDOW_X_CENTER + (i - (TILE_NUMBER / 2)) * 50, WINDOW_Y_CENTER + (j - ((TILE_NUMBER - 6) / 2)) * 50, false);
				}
				else
				{
					objectGroup[i][j].Init(WINDOW_X_CENTER + (i - (TILE_NUMBER / 2)) * 50, WINDOW_Y_CENTER + (j - ((TILE_NUMBER - 6) / 2)) * 50, false);
				}
			}
		}

		hero.Init();

		magazine.resize(6);

		for (int i = 0; i < 6; i++)
			magazine[i].Init(hero.getPosition().x + 3 * magazine[i].getSize() * cos(PI * (i + 1) / 3), hero.getPosition().y + 3 * magazine[i].getSize() * sin(PI * (i + 1) / 3));

		SetTimer(hWnd, 0, 1, NULL);
	}
	break;
	case WM_GETMINMAXINFO:
	{
		((MINMAXINFO*)lParam)->ptMaxTrackSize.x = WINDOW_X;
		((MINMAXINFO*)lParam)->ptMaxTrackSize.y = WINDOW_Y;
		((MINMAXINFO*)lParam)->ptMinTrackSize.x = WINDOW_X;
		((MINMAXINFO*)lParam)->ptMinTrackSize.y = WINDOW_Y;
	}
	break;
	case WM_TIMER:
	{
		switch (lParam)
		{
		case 0:
		{
			if (hero.getLife() == 0)
			{
				KillTimer(hWnd, 0);
				MessageBox(hWnd, "플레이어 목숨 고갈. 게임을 종료합니다...", "사망 정보", MB_OK);
				PostQuitMessage(0);
			}

			if (hero.getLife() > 0)
			{
				g_PrevKilledEnemyNumber = g_KilledEnemyNumber;
				if (g_KilledEnemyNumber > 0)
				{
					g_ComboTime++;
				}

				if (g_ComboTime > 350)
				{
					if (g_KilledEnemyNumber == g_PrevKilledEnemyNumber)
					{
						g_KilledEnemyNumber = 0;
						g_ComboTime = 0;
						g_ComboScore = 0;
					}
				}
				
				if (hero.getMustRespawn())
				{
					for (int j = 0; j < TILE_NUMBER - 6; j++)
					{
						for (int i = 0; i < TILE_NUMBER; i++)
						{
							if (CheckBreakTileByRespawn(hero, objectGroup[i][j]))
							{
								objectGroup[i][j].setExplosable(false);
							}
						}
					}

					for (auto& l : enemyList)
					{
						if (l.getVisible())
						{
							if (CheckKillEnemyByRespawn(hero, l))
							{
								l.ExplosionEffect(l.getPosition().x, l.getPosition().y, true);
								l.setVisible(false);
							}
						}
					}
				}

				g_RespawnTime++;
				if (g_RespawnTime > 500)
				{
					hero.setMustRespawn(false);
				}

				////////////////////////////////////////////////////////////////////////// 타일들의 업데이트
				for (int j = 0; j < TILE_NUMBER - 6; j++)
				{
					for (int i = 0; i < TILE_NUMBER; i++)
					{
						objectGroup[i][j].Update();
					}
				}

				////////////////////////////////////////////////////////////////////////// 플레이어 업데이트

				if (hero.getLife() > 0 && hero.getMustExplosive() == false)
				{
					hero.Update();

					////////////////////////////////////////////////////////////////////////// 플레이어와 검은 타일의 충돌
					for (int j = 0; j < TILE_NUMBER - 6; j++)
					{
						for (int i = 0; i < TILE_NUMBER; i++)
						{
							if (CheckCollisionHeroNObject(hero, objectGroup[i][j]) && objectGroup[i][j].getExplosable())
							{
								if (GetAsyncKeyState('W') & 0x8000 || GetAsyncKeyState('w') & 0x8000)
								{
									hero.MoveDown();
								}
								if (GetAsyncKeyState('S') & 0x8000 || GetAsyncKeyState('s') & 0x8000)
								{
									hero.MoveUp();
								}
								if (GetAsyncKeyState('A') & 0x8000 || GetAsyncKeyState('a') & 0x8000)
								{
									hero.MoveRight();
								}
								if (GetAsyncKeyState('D') & 0x8000 || GetAsyncKeyState('d') & 0x8000)
								{
									hero.MoveLeft();
								}
							}
						}
					}
				}

				if (!playerBulletList.empty())
				{
					for (auto& l : playerBulletList)
					{
						for (int j = 0; j < TILE_NUMBER - 6; j++)
						{
							for (int i = 0; i < TILE_NUMBER; i++)
							{
								if (CheckCollisionBulletNObject(l, objectGroup[i][j]))
								{
									// 총알과 오브젝트 충돌
									if (objectGroup[i][j].getInvinsible())
									{
										g_Score -= 100;
										l.setVisible(false);
										l.setDirection(NONE);
									}
									else
									{
										g_Score += 50;
										objectGroup[i][j].setExplosable(false);
									}
								}
							}
						}

						l.Update();
					}
				}

				if (!specialBulletList.empty())
				{
					for (auto& s : specialBulletList)
					{
						if (CheckCollisionBulletNHero(s, hero))
						{
							s.setVisible(false);
							s.setActive(false);

							// 특수총알 먹었을 때
						}
						s.Update();
					}
				}

				////////////////////////////////////////////////////////////////////////// 플레이어가 가지고 있는 탄창 업데이트
				{
					g_Time++;
					if (g_Time > 150)
					{
						if (g_BulletCount < 6)
						{
							magazine[g_BulletCount].setVisible(true);
							g_BulletCount++;
						}
						g_Time = 0;
					}

					g_PlayTime++;
					g_TimeEnemyCreate++;

					if (g_PlayTime > 0 && g_PlayTime < 1500)
					{
						if (g_TimeEnemyCreate % 350 == 0)
						{
							CreateEnemy();
						}
					}
					else if (g_PlayTime > 1501 && g_PlayTime < 3000)
					{
						if (g_TimeEnemyCreate % 250 == 0)
						{
							CreateEnemy();
						}
					}
					else if (g_PlayTime > 3001 && g_PlayTime < 4500)
					{
						if (g_TimeEnemyCreate % 150 == 0)
						{
							CreateEnemy();
						}
					}
					else
					{
						if (g_TimeEnemyCreate % 100 == 0)
						{
							CreateEnemy();
						}
					}


					if (hero.getMustExplosive())
						hero.UpdateExplosion();

					if (hero.getMustRespawn() == false)
					{
						if (!enemyList.empty())
						{
							for (auto& l : enemyList)
							{
								if (l.getVisible())
								{
									if (CheckCollisionHeroNEnemy(hero, l))
									{
										g_Score -= 500;
										hero.ExplosionEffect(hero.getPosition().x, hero.getPosition().y, true);
									}

									if (!playerBulletList.empty())
									{
										for (auto& r : playerBulletList)
										{
											if (CheckCollisionBulletNEnemy(r, l))
											{
												l.ExplosionEffect(l.getPosition().x, l.getPosition().y, true);
												l.setVisible(false);
												r.setVisible(false);
												r.setDirection(NONE);
												for (int j = 0; j < TILE_NUMBER - 6; j++)
												{
													for (int i = 0; i < TILE_NUMBER; i++)
													{
														if (CheckBreakEnemyNTile(l, objectGroup[i][j]))
														{
															if (objectGroup[i][j].getExplosable() == true)
																g_Score += 50;
															objectGroup[i][j].setExplosable(false);
														}
													}
												}

												CreateSpecialBullet(r.getPosition().x, r.getPosition().y);
											}
										}
									}

									l.Update(hero.getPosition().x, hero.getPosition().y);
								}
								else
								{
									l.UpdateExplosion();
								}
							}
						}
					}
				

					for (auto& l : enemyList)
					{
						for (auto& r : enemyList)
						{
							if (CheckCollisionEnemyNEnemy(l, r))
							{
								g_KilledEnemyNumber++;
								r.ExplosionEffect(r.getPosition().x, r.getPosition().y, true);
								r.setVisible(false);

								for (int j = 0; j < TILE_NUMBER - 6; j++)
								{
									for (int i = 0; i < TILE_NUMBER; i++)
									{
										if (CheckBreakEnemyNTile(l, objectGroup[i][j]))
										{
											objectGroup[i][j].setExplosable(false);
										}
									}
								}

								CreateSpecialBullet(r.getPosition().x, r.getPosition().y);
							}
						}
					}

					for (int k = 0; k < 6; k++)
					{
						magazine[k].VariateAngle(7);
						magazine[k].VariateRadian(PI * magazine[k].getAngle() / 180);
						int newX = hero.getPosition().x + 3 * magazine[k].getSize() * cos(PI * (k + 1) / 3 + magazine[k].getRadian());
						int newY = hero.getPosition().y + 3 * magazine[k].getSize() * sin(PI * (k + 1) / 3 + magazine[k].getRadian());
						magazine[k].setPosition(newX, newY);
					}
				}
			}
		}
		break;
		}

		InvalidateRgn(hWnd, NULL, false);
	}
	break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hWnd, &ps);

		memdc = CreateCompatibleDC(hdc);
		hBitmap = CreateCompatibleBitmap(hdc, crt.right, crt.bottom);
		hOldBitmap = (HBITMAP)SelectObject(memdc, hBitmap);
		FillRect(memdc, &crt, (HBRUSH)GetStockObject(WHITE_BRUSH)); //도화지 색 변경

		// TODO:
		for (int j = 0; j < TILE_NUMBER - 6; j++)
		{
			for (int i = 0; i < TILE_NUMBER; i++)
			{
				objectGroup[i][j].Render(memdc);
			}
		}

		if (!playerBulletList.empty())
		{
			for (auto& l : playerBulletList)
			{
				l.Render(memdc);
			}
		}

		if (hero.getMustExplosive())
			hero.RenderExlosion(memdc);
	
		if(hero.getLife() > 0)
			hero.Render(memdc);
		
		if (hero.getLife() > 0)
		{
			if (hero.getMustExplosive() == false)
			{
				if (!magazine.empty())
				{
					for (int i = 0; i < g_BulletCount; i++)
					{
						magazine[i].Render(memdc);
					}
				}
			}
		}

		if (!enemyList.empty())
		{
			for (auto& l : enemyList)
			{
				if (l.getVisible())
				{
					l.Render(memdc);
				}
				else
				{
					l.RenderExlosion(memdc);
				}
			}
		}

		if (!specialBulletList.empty())
		{
			for (auto& s : specialBulletList)
			{
				if (s.getVisible())
				{
					s.Render(memdc);
				}
			}
		}

		// UI 출력
		hFont = CreateFont(40, 0, 0, 0, 800, 0, 0, 0, HANGEUL_CHARSET, 0, 0, 0, 0, TEXT("맑은 고딕"));
		hOldFont = (HFONT)SelectObject(memdc, hFont);

		sprintf_s(str, "남은목숨 : %d   스테이지 : %d   ", hero.getLife(), g_Stage);
		TextOut(memdc, 50, 740, str, strlen(str));

		if(isInvincibleMode)
			sprintf_s(str2, "무적모드(F1키) : ON");
		else
			sprintf_s(str2, "무적모드(F1키) : OFF");
		TextOut(memdc, WINDOW_X_CENTER - 160, 740, str2, strlen(str2));

		sprintf_s(str3, "점수 : %d", g_Score);
		TextOut(memdc, WINDOW_X_CENTER + 160, 740, str3, strlen(str3));

		sprintf_s(str4, "콤보 : %d", g_ComboScore);
		TextOut(memdc, WINDOW_X_CENTER + 400, 740, str4, strlen(str4));

		SelectObject(memdc, hOldFont);
		DeleteObject(hFont);

		BitBlt(hdc, 0, 0, crt.right, crt.bottom, memdc, 0, 0, SRCCOPY);
		DeleteObject(SelectObject(memdc, hOldBitmap));
		DeleteDC(memdc);

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_CHAR:
	{
		switch (wParam)
		{
		case '1':
		{
			g_Stage = 1; // 하 난이도
			ChangeStage();
		}
		break;
		case '2':
		{
			g_Stage = 2; // 중 난이도
			ChangeStage();
		}
		break;
		case '3':
		{
			g_Stage = 3; // 상 난이도
			ChangeStage();
		}
		break;
		case 'r': case 'R': // 무적 모드
		{
		}
		break;
		}
	}
	break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_F1:
		{
			if (isInvincibleMode)
				isInvincibleMode = false;
			else
				isInvincibleMode = true;
		}
		break;
		case VK_ESCAPE:
		{
			PostQuitMessage(0);
		}
		break;
		case VK_UP:
		{
			if (g_BulletCount > 0)
			{
				g_BulletCount--;
				hero.setDirection(UP);
				magazine[g_BulletCount].setVisible(false);
				hero.ShootBullet();
			}
		}
		break;
		case VK_DOWN:
		{
			if (g_BulletCount > 0)
			{
				g_BulletCount--;
				hero.setDirection(DOWN);
				magazine[g_BulletCount].setVisible(false);
				hero.ShootBullet();
			}
		}
		break;
		case VK_LEFT:
		{
			if (g_BulletCount > 0)
			{
				g_BulletCount--;
				hero.setDirection(LEFT);
				magazine[g_BulletCount].setVisible(false);
				hero.ShootBullet();
			}
		}
		break;
		case VK_RIGHT:
		{
			if (g_BulletCount > 0)
			{
				g_BulletCount--;
				hero.setDirection(RIGHT);
				magazine[g_BulletCount].setVisible(false);
				hero.ShootBullet();
			}
		}
		break;
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		int mousex = LOWORD(lParam);
		int mousey = HIWORD(lParam);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}